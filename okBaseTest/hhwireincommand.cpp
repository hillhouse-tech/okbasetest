#include "hhwireincommand.h"
#include "frontpanel.h"

HHWireinCommand::HHWireinCommand(const QString& name)
    :HHCommandBase(name)
{

}

void HHWireinCommand::execute()
{
    FrontPanel::getInstance()->wireIn(mAddress, mValue, mMask);
}

void HHWireinCommand::setAddress(uint32_t address)
{
    mAddress = address;
}

void HHWireinCommand::setMask(uint32_t mask)
{
    mMask = mask;
}

void HHWireinCommand::setValue(uint32_t  value)
{
    mValue = value;
}

HHCommandBase *HHWireinCommand::clone()
{
    HHWireinCommand* pCmd = new HHWireinCommand(this->mName);
    pCmd->valid(this->valid());
    pCmd->needsArg(this ->needsArg());
    pCmd->error(this->error());
    pCmd->mAddress = this->mAddress;
    pCmd->mValue = this->mValue;
    pCmd->mMask = this->mMask;

    return pCmd;
}
