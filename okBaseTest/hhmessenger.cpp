#include "hhmessenger.h"
#include "hhmessageevent.h"
#include <QCoreApplication>
#include <QWidget>

HHMessenger* HHMessenger::getInstance()
{
    if (!spInstance)
    {
        spInstance = new HHMessenger;
    }
    return spInstance;
}

void HHMessenger::setWindow(QWidget* pWindow)
{
    mpWindow = pWindow;
}

HHMessenger::HHMessenger()
    :mpWindow(nullptr)
{
}

void HHMessenger::sendMessage(QString msg, EMessageType type)
{
    if (mpWindow)
        QCoreApplication::postEvent(mpWindow, new HHMessageEvent(msg, type));
}

HHMessenger* HHMessenger::spInstance = nullptr;
