#ifndef HHDISPLAYWIDGET_H
#define HHDISPLAYWIDGET_H

#include <QWidget>
#include <QPixmap>

class HHDisplayWidget : public QWidget
{
    Q_OBJECT
public:
    explicit HHDisplayWidget(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *event);

public slots:
    void handleUpdateDisplay(const QImage& image);

private:
    QPixmap mPixelMap;
};

#endif // HHDISPLAYWIDGET_H
