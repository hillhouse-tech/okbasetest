#include "hhdisplaywidget.h"
#include "hhconstants.h"
#include <QPainter>

HHDisplayWidget::HHDisplayWidget(QWidget *parent)
    : QWidget(parent)
{
}

void HHDisplayWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.drawPixmap(QPoint(), mPixelMap);
}

void HHDisplayWidget::handleUpdateDisplay(const QImage& image)
{
    if (image.isNull()) return;

//    mPixelMap = QPixmap::fromImage(image);
    mPixelMap = QPixmap::fromImage(image.scaled(this->size()));
    repaint();
}
