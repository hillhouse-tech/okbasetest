#ifndef HHWIREINCOMMAND_H
#define HHWIREINCOMMAND_H

#include <stdint.h>
#include "hhcommand.h"

class FrontPanel;

class HHWireinCommand : public HHCommandBase
{
public:
    HHWireinCommand(const QString& name);

    virtual void execute() override;

    virtual void setValue(uint32_t value) override;
    void setAddress(uint32_t address);
    void setMask(uint32_t mask);

    virtual HHCommandBase* clone() override;

private:
    uint32_t mAddress;
    uint32_t mValue;
    uint32_t mMask;
};

#endif // HHWIREINCOMMAND_H
