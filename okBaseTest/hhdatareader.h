#ifndef HHDATAREADER_H
#define HHDATAREADER_H

#include "hhconstants.h"

class HHDataReader
{
public:
    static unsigned int getCol(unsigned char data[EVENT_SIZE]);
    static unsigned int getRow(unsigned char data[EVENT_SIZE]);
    static unsigned int getTimeStamp(unsigned char data[EVENT_SIZE]);
    static unsigned int getADC(unsigned char data[EVENT_SIZE]);

    // For Event-only Mode
    static unsigned int getSensorTime(unsigned char data[EVENT_SIZE]);
    static unsigned int getPixelTag(unsigned char data[EVENT_SIZE]);
    static unsigned int getSpecialEventTag(unsigned char data[EVENT_SIZE]);
    static unsigned int getColTag(unsigned char data[EVENT_SIZE]);

    //check event type
    static bool isForcefirePixel(unsigned char data[EVENT_SIZE]); // column event from force fire, carrying ADC
    static bool isCol(unsigned char data[EVENT_SIZE]);
    static bool isRow(unsigned char data[EVENT_SIZE]);
    static bool isSpecialEvent(unsigned char data[EVENT_SIZE]);

    static unsigned int sCurRow;
    static unsigned int sCurT;
};

#endif // HHDATAREADER_H
