#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <stdint.h>
#include <QFile>
#include <QDataStream>
#include <fstream>
#include "hhrenderthread.h"

namespace Ui {
class MainWindow;
}

class FrontPanel;
class HHSequenceMgr;
enum class EMessageType;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setFrontPanel(FrontPanel* pFrontPanel);
    void appendMessage(QString msg, EMessageType type = (EMessageType)0);
    void showDialog();

    // system command impl
    // the bias comibinations
    void setThreshold(uint32_t value);
    void setBrightness(uint32_t value);
    void setContrast(uint32_t value);
    void setSample(uint32_t value);
    void setRecording(bool bStart);
    void playback();
    void reinitializeFPGA(uint32_t value);
    void generateFPN(uint32_t value);
    void adjustBrightness(uint32_t value);
    void setUpperADC(uint32_t value);
    void setLowerADC(uint32_t value);

protected:
    bool event(QEvent *event);

private:
    void addToDialog(const QString& name);
    void refreshDialog();
    void powerUp();
    void loadInitialSettings();

signals:
    void updateMainDisplay(const QImage&);
    void updateSecondDisplay(const QImage&);

private slots:
    void handlePushButton();
    void handleSliderValueChanged(uint32_t);
    void handlePipeoutTimer();
    void handlePlaybackTimer();
    void handleRenderedImage(const QImage& image, int whichDisplay);
    void handleCbSelect(int index);
    void handleCommandCallback(QString info);

private:
    Ui::MainWindow *ui;
    QDialog* mpDialog;
    FrontPanel* mpFrontPanel;
    HHSequenceMgr* mpSeqMgr;
    QHash<QString, uint32_t> mSliderNameValueMap;
    QVector<QString> mAdvancedNames; // to load bias
    int mDialogRow;
    int mDialogCol;
    QTimer* mpPipeOutTimer;
    QFile* mpRecordFile;
    QDataStream* mpRecordStream;
    QTimer* mpPlaybackTimer;
    std::ifstream mPlaybackStream;
    int mMode;
    bool mPipeoutAllowed;

    HHRenderThread mRenderThread;
    unsigned char* mpData;
    bool mIsRecording;
};

#endif // MAINWINDOW_H
