#ifndef HHSEQUENCEMGR_H
#define HHSEQUENCEMGR_H

#include <QString>
#include <vector>
#include <QHash>
#include <QVector>
#include <stdint.h>

class HHCommandBase;

class HHSequence
{
public:
    HHSequence(const QString& name);
    virtual ~HHSequence();

    QString name();
    bool isShown();
    void setShow(bool bShow);
    bool isAdvanced();
    void setAdvanced(bool bAdvanced);
    void addCommand(HHCommandBase* pCmd);
    void setNext(const QString& nextName);
    QString getNext();
    virtual bool fire();

private:
    QString mName;
    QString mNext;
    bool mShow;
    bool mAdvanced;

protected:
    QVector<HHCommandBase*> mCommands;
};

class HHSequenceSlider : public HHSequence
{
public:
    HHSequenceSlider(const QString& name, uint32_t min, uint32_t max);

    uint32_t getMax();
    uint32_t getMin();
    uint32_t getValue();
    void setValue(uint32_t value);

    bool fireWithArg(uint32_t newValue);

private:
    uint32_t mMin;
    uint32_t mMax;
    uint32_t mValue;
};

class HHSequenceMgr
{
public:
    HHSequenceMgr();
    ~HHSequenceMgr();

    bool initCommandList();
    bool initSequenceList();
    bool initSliderList();

    HHCommandBase* getCommandByName(const QString& name);
    HHSequence* getSequenceByName(const QString& name);
    HHSequenceSlider* getSliderByName(const QString& name);

    QVector<QString> getAllSequenceNames();
    QVector<QString> getAllSliderNames();

private:
    QVector<HHCommandBase*> mCommandList;
    QHash<QString, HHCommandBase*> mCommandMap;
    QVector<HHSequence*> mSequenceList;
    QHash<QString, HHSequence*> mSequenceMap;
    QVector<HHSequence*> mSliderList;
    QHash<QString, HHSequence*> mSliderMap;
};

#endif // HHSEQUENCEMGR_H
