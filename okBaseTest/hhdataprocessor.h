#ifndef HHDATAPROCESSOR_H
#define HHDATAPROCESSOR_H

#include <QImage>
#include <QObject>
#include <QFile>
#include <stdint.h>

class HHDataProcessor : public QObject
{
    Q_OBJECT
public:
    HHDataProcessor();
    ~HHDataProcessor();
    bool setFpnFile(const QString& fpnFile);
    void processData(unsigned char* data, long length);
    bool processEvent(unsigned char* data); // return true for normal event, false for special event
    void processPlaybackData(unsigned char* data, long length);
    void createImage();
    void cleanBuffer();
    QImage getImage0();
    QImage getImage1();
    void setMode(int mode);
    void setImageSelect(int whichImage, int select);
    void generateFPN();
    void adjustBrightness();
    void setUpperADC(uint32_t value);
    void setLowerADC(uint32_t value);

private:
    void generateFPNimpl();
    void adjustBrightnessImpl();

signals:
    void imageReady(int whichDisplay);
    void commandCallback(QString info);

public:
    static const int COLORMAP72[72][3];

private:
    unsigned char* mpFullPicBuffer;
    unsigned char* mpEventPicBuffer;
    unsigned char* mpMarkPicBuffer;
    unsigned char* mpOpticalFlowBuffer;
    long* mpFpnGenerationBuffer;
    // unsigned char* mpSpeedBuffer;
    char* mpFpnBuffer;
    bool mAligned;
    int mMode; // for different .bit
    int mImageSelect0;
    int mImageSelect1;
    QImage mImage0;
    QImage mImage1;
    int mSpecialEventCount;
    bool mIsGeneratingFPN;
    int mFpnCalculationTimes;
    bool mAdjustBrightness;
    uint32_t mUpperADC;
    uint32_t mLowerADC;
    //
    QMutex mImageMux;
    QMutex mModeMux;
};

#endif // HHDATAPROCESSOR_H
