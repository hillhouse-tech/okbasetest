#ifndef HHXMLREADER_H
#define HHXMLREADER_H

#include <vector>
#include <QString>
#include <stdint.h>

class QDomDocument;
class HHCommandBase;
class HHSequence;
class HHSequenceMgr;

class HHXmlReader
{
public:
    HHXmlReader();
    ~HHXmlReader();

    bool parse(const QString& filename, QDomDocument* pDom);
    bool importCommands(QVector<HHCommandBase*>& commandList, QDomDocument* pDom);
    bool importSequences(HHSequenceMgr* pSeqMgr, QVector<HHSequence*>& sequenceList, QDomDocument* pDom);
    bool importSliders(HHSequenceMgr* pSeqMgr, QVector<HHSequence*>& sliderList, QDomDocument* pDom);

 private:
    bool getNumber(const QString& text, uint32_t* pNumber);
};

#endif // HHXMLREADER_H
