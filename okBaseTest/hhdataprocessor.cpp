#include "hhdataprocessor.h"
#include "hhconstants.h"
#include "hhdatareader.h"
#include <QCoreApplication>
#include <QTextStream>
#include <QDialog>
#include <iostream>
#include <fstream>
#include <string>

HHDataProcessor::HHDataProcessor()
    :mAligned(false), mSpecialEventCount(0)
    , mIsGeneratingFPN(false), mFpnCalculationTimes(0), mAdjustBrightness(false)
    , mUpperADC(511), mLowerADC(0)
    , mMode(0)
    , mImage0(PIXELS_PER_COL, PIXELS_PER_ROW, QImage::Format_RGB32)
    , mImage1(PIXELS_PER_COL, PIXELS_PER_ROW, QImage::Format_RGB32)
    , mImageSelect0(0)
    , mImageSelect1(1)
{
    mpFullPicBuffer = new unsigned char[PIXELS_NUMBER];
    mpEventPicBuffer = new unsigned char[PIXELS_NUMBER];
    mpMarkPicBuffer = new unsigned char[PIXELS_NUMBER];
    mpOpticalFlowBuffer = new unsigned char[PIXELS_NUMBER];
    // mpSpeedBuffer = new unsigned char[PIXELS_NUMBER];
    mpFpnBuffer = new char[PIXELS_NUMBER];
    mpFpnGenerationBuffer = new long[PIXELS_NUMBER];
    for (int i = 0; i < PIXELS_NUMBER; ++i) mpFpnBuffer[i] = 0;
}

HHDataProcessor::~HHDataProcessor()
{
    delete [] mpFullPicBuffer;
    delete [] mpEventPicBuffer;
    delete [] mpMarkPicBuffer;
    delete [] mpOpticalFlowBuffer;
    // delete [] mpSpeedBuffer;
    delete [] mpFpnBuffer;
    delete [] mpFpnGenerationBuffer;
}

bool HHDataProcessor::setFpnFile(const QString &fpnFile)
{
    QString filepath = QCoreApplication::applicationDirPath() + "/" + fpnFile;
    int index = 0;
    std::ifstream in;
    in.open(filepath.toStdString());
    if (!in.is_open()) {
        return false;
    }

    std::string line;
    while (!in.eof() && index < PIXELS_NUMBER) {
        in >> line;
        mpFpnBuffer[index] = std::stoi(line);
        index++;
    }
    if (index != PIXELS_NUMBER)
        return false;

    return true;
}

void HHDataProcessor::processPlaybackData(unsigned char* data, long length)
{
    if (length != 4)
        return;

    bool isSpecialEvent = !(processEvent(data));
    if (isSpecialEvent)
    {
        ++mSpecialEventCount;

        if (mSpecialEventCount >= SE_TILL_RENDER)
        {
            createImage();

            if (mImageSelect0 >= 0)
            {
                emit imageReady(0);
            }
            if (mImageSelect1 >= 0)
            {
                emit imageReady(1);
            }

            cleanBuffer();
            mSpecialEventCount = 0;
        }
        else
        {
            cleanBuffer();
        }
    }
}

void HHDataProcessor::processData(unsigned char *data, long length)
{
    long i = 0;
    if (!mAligned)
    {
        while (i + 1 < length)
        {
            if ((data[i] & 0x80) > 0 && (data[i + 1] & 0x80) == 0x00)
            {
                i = i - 3;
                // init sRow and sT
                processEvent(&data[i]);
                break; // aligned
            }
            ++i;
        }

        //
        for (i = i + EVENT_SIZE; i + 11 < length; i = i + EVENT_SIZE)
        {
            bool isSpecialEvent = !(processEvent(&data[i]));
            if (isSpecialEvent)
            {
                ++mSpecialEventCount;

                if (mSpecialEventCount >= SE_TILL_RENDER)
                {
                    if (mAdjustBrightness)
                    {
                        adjustBrightnessImpl();
                    }
                    else if (mIsGeneratingFPN)
                    {
                        generateFPNimpl();
                    }
                    else
                    {
                        createImage();

                        if (mImageSelect0 >= 0)
                        {
                            emit imageReady(0);
                        }
                        if (mImageSelect1 >= 0)
                        {
                            emit imageReady(1);
                        }
                    }

                    cleanBuffer();
                    mSpecialEventCount = 0;
                }
                else
                {
                   cleanBuffer();
                }
            }
        }
    }
}

bool HHDataProcessor::processEvent(unsigned char *data)
{
    if (HHDataReader::isSpecialEvent(data))
    {
        return false;
    }

    unsigned int row;
    unsigned int col;
    unsigned int adc;
    unsigned int t;
    if (HHDataReader::isRow(data))
    {
        HHDataReader::sCurRow = HHDataReader::getRow(data);
        HHDataReader::sCurT = HHDataReader::getTimeStamp(data);

        return true;
    }

    QMutexLocker lock(&mModeMux);

    //
    // col event
    //
    if (mMode == 0)
    {
        row = HHDataReader::sCurRow;
        col = HHDataReader::getCol(data);
        adc = HHDataReader::getADC(data);
        t = HHDataReader::sCurT;

        if (row < PIXELS_PER_ROW && col < PIXELS_PER_COL)
        {
            int index = row * PIXELS_PER_COL + col;

            // normalize to 0~255
            if (adc < mLowerADC) adc = 0;
            else if (adc > mUpperADC) adc = 255;
            else adc = 255.0 * (adc - mLowerADC) / (mUpperADC - mLowerADC);

            mpFullPicBuffer[index] = adc;
            mpEventPicBuffer[index] += 1;
        }
        else
        {

        }
    }
    else if (mMode == 1 || mMode == 2)
    {
        row = HHDataReader::sCurRow;
        col = HHDataReader::getCol(data);
        if (HHDataReader::isForcefirePixel(data))
        {
            adc = HHDataReader::getADC(data);
            t = 0;
        }
        else // time info
        {
            t = HHDataReader::getADC(data);
            adc = 0;
        }

        if (row < PIXELS_PER_ROW && col < PIXELS_PER_COL)
        {
            int index = row * PIXELS_PER_COL + col;

            if (adc > 0)
            {
                // normalize to 0~255
                if (adc < mLowerADC) adc = 0;
                else if (adc > mUpperADC) adc = 255;
                else adc = 255.0 * (adc - mLowerADC) / (mUpperADC - mLowerADC);

                if (adc > 255)
                {
                    adc = 0;
                }
                mpFullPicBuffer[index] = adc;
            }
            if (t > 0 && mpOpticalFlowBuffer[index] <= 0)
            {
                mpOpticalFlowBuffer[index] = t / 2;
                mpEventPicBuffer[index] += 1;
            }
        }
    }

    return true;
}

void HHDataProcessor::createImage()
{
    QMutexLocker lock(&mImageMux);
    int col = 0;
    int row = 0;
    for (int i = 0; i < PIXELS_NUMBER; ++i)
    {
        int value;
        QRgb rgb;
        //
        // full pic
        //
        if (mImageSelect0 == 0 || mImageSelect1 == 0)
        {
            value = mpFullPicBuffer[i];
            // add FPN
            value = value - mpFpnBuffer[i];
            if (value < 0) value = 0;
            if (value > 255) value = 255;
            rgb = qRgb(value, value, value);

            if (mImageSelect0 == 0)
                mImage0.setPixel(col, row, rgb);
            if (mImageSelect1 == 0)
                mImage1.setPixel(col, row, rgb);
        }

        //
        // evt pic
        //
        if (mImageSelect0 == 1 || mImageSelect1 == 1)
        {
            value = mpEventPicBuffer[i];
            if (value > 0)
                rgb = qRgb(255, 255, 255);
            else
                rgb = qRgb(0, 0, 0);

            if (mImageSelect0 == 1)
                mImage0.setPixel(col, row, rgb);
            if (mImageSelect1 == 1)
                mImage1.setPixel(col, row, rgb);
        }

        //
        // mark pic
        //
        if (mImageSelect0 == 2 || mImageSelect1 == 2)
        {
            value = mpFullPicBuffer[i];
            if (mpEventPicBuffer[i] > 0)
                rgb = qRgb(COLORMAP72[36][0], COLORMAP72[36][1], COLORMAP72[36][2]);
            else
                rgb = qRgb(value, value, value);

            if (mImageSelect0 == 2)
                mImage0.setPixel(col, row, rgb);
            if (mImageSelect1 == 2)
                mImage1.setPixel(col, row, rgb);
        }

        //
        // optical flow 0
        //
        if (mImageSelect0 == 3 || mImageSelect1 == 3)
        {
            value = mpOpticalFlowBuffer[i];
            int k = static_cast<int>(72.0 * value / 256);
            if (k > 0)
                rgb = qRgb(COLORMAP72[k][0], COLORMAP72[k][1], COLORMAP72[k][2]);
            else
                rgb = qRgb(0, 0, 0);

            if (mImageSelect0 == 3)
                mImage0.setPixel(col, row, rgb);
            if (mImageSelect1 == 3)
                mImage1.setPixel(col, row, rgb);
        }

        //
        // optical flow 1 (speed)
        //
        if (mImageSelect0 == 4 || mImageSelect1 == 4)
        {
            if (col <= 0 || col >= PIXELS_PER_COL - 1 || row <=0 || row >= PIXELS_PER_ROW - 1)
            {
                // edge point
                rgb = qRgb(0, 0, 0);
            }
            else
            {
                value = mpOpticalFlowBuffer[i];
                int k;

                unsigned char x1 = mpOpticalFlowBuffer[i - 1];
                unsigned char x2 = mpOpticalFlowBuffer[i + 1];
                unsigned char y1 = mpOpticalFlowBuffer[i - PIXELS_PER_COL];
                unsigned char y2 = mpOpticalFlowBuffer[i + PIXELS_PER_COL];

                bool bRemoveEdge = false;

                if (x1 == 0 && x2 == 0 && y1 == 0 && y2 == 0)
                {
                    k = 0;
                }
                else if (bRemoveEdge && (x1 == 0 || x2 == 0 || y1 == 0 || y2 == 0))
                {
                    k = 0;
                }
                else
                {
                    int dx = x2 - x1; if (dx < 1) dx = 1;
                    int dy = y2 - y1; if (dy < 1) dy = 1;
                    int vx = 255 / dx;
                    int vy = 255 / dy;
                    int vxy = std::sqrt(vx * vx + vy * vy);
                    double thresh = 361.0;
                    k = vxy / thresh * 72;
                }

                if (k > 0)
                    rgb = qRgb(COLORMAP72[k][0], COLORMAP72[k][1], COLORMAP72[k][2]);
                else
                    rgb = qRgb(0, 0, 0);
            }

            if (mImageSelect0 == 4)
                mImage0.setPixel(col, row, rgb);
            if (mImageSelect1 == 4)
                mImage1.setPixel(col, row, rgb);
        }

        ++col;
        if (col >= PIXELS_PER_COL)
        {
            ++row;
            col = 0;
        }
    }

    bool mirrorH = (MIRROR_HORIZONTAL % 2 == 1);
    bool mirrorV = (MIRROR_VERTICAL % 2 == 1);
    mImage0 = mImage0.mirrored(mirrorH, mirrorV);
    mImage1 = mImage1.mirrored(mirrorH, mirrorV);
}

void HHDataProcessor::cleanBuffer()
{
    for (int i = 0; i < PIXELS_NUMBER; ++i)
    {
        mpEventPicBuffer[i] = 0;
        mpOpticalFlowBuffer[i] = 0;
        // mpSpeedBuffer[i] = 0;
    }
}

QImage HHDataProcessor::getImage0()
{
    QMutexLocker lock(&mImageMux);
    return mImage0.copy();
}

QImage HHDataProcessor::getImage1()
{
    QMutexLocker lock(&mImageMux);
    return mImage1.copy();
}

void HHDataProcessor::setMode(int mode)
{
    QMutexLocker lock(&mModeMux);
    mMode = mode;
}

void HHDataProcessor::setImageSelect(int whichImage, int select)
{
    QMutexLocker lock(&mImageMux);
    if (whichImage == 0)
    {
        mImageSelect0 = select;
    }
    else if (whichImage == 1)
    {
        mImageSelect1 = select;
    }
}

void HHDataProcessor::generateFPN()
{
    mIsGeneratingFPN = true;
    mFpnCalculationTimes = FPN_CALCULATION_TIMES;
}

void HHDataProcessor::adjustBrightness()
{
    mAdjustBrightness = (!mAdjustBrightness);
}

void HHDataProcessor::setUpperADC(uint32_t value)
{
    mUpperADC = value;
}

void HHDataProcessor::setLowerADC(uint32_t value)
{
    mLowerADC = value;
}

void HHDataProcessor::generateFPNimpl()
{
    for (int i = 0; i < PIXELS_NUMBER; ++i)
    {
        int b = mpFullPicBuffer[i]; Q_UNUSED(b);
        if (mFpnCalculationTimes == FPN_CALCULATION_TIMES)
            mpFpnGenerationBuffer[i] = mpFullPicBuffer[i];
        else
            mpFpnGenerationBuffer[i] += mpFullPicBuffer[i];
    }

    --mFpnCalculationTimes;

    if (mFpnCalculationTimes <= 0)
    {
        mIsGeneratingFPN = false;

        // output the FPN file now
        QFile f( QCoreApplication::applicationDirPath() + "/FPN.txt");

        if (!f.open(QIODevice::WriteOnly | QIODevice::Text))
            return;

        QTextStream ss(&f);

        long total = 0;
        for (int i = 0; i < PIXELS_NUMBER; ++i)
        {
            mpFpnGenerationBuffer[i] = mpFpnGenerationBuffer[i] / FPN_CALCULATION_TIMES;
            total += mpFpnGenerationBuffer[i];
        }

        int avg = total / PIXELS_NUMBER;

        for (int i = 0; i < PIXELS_NUMBER; ++i)
        {
            int a = mpFpnGenerationBuffer[i]; Q_UNUSED(a);
            int d = mpFpnGenerationBuffer[i] - avg;
            ss << d; ss << "\n";
        }
        f.close();

        emit commandCallback("fpn_done");
    }

}

void HHDataProcessor::adjustBrightnessImpl()
{
    long total = 0;
    for (int i = 0; i < PIXELS_NUMBER; ++i)
    {
        total += mpFullPicBuffer[i];
    }
    long avg = total / PIXELS_NUMBER;

    if (avg < 118)
    {
        emit commandCallback("up_brightness");
    }
    if (avg > 138)
    {
        emit commandCallback("down_brightness");
    }
}

const int HHDataProcessor::COLORMAP72[72][3] = {
    {  0,   0, 127},
    {  0,   0, 141},
    {  0,   0, 155},
    {  0,   0, 169},
    {  0,   0, 183},
    {  0,   0, 198},
    {  0,   0, 212},
    {  0,   0, 226},
    {  0,   0, 240},
    {  0,   0, 255},
    {  0,  14, 255},
    {  0,  28, 255},
    {  0,  42, 255},
    {  0,  56, 255},
    {  0,  70, 255},
    {  0,  84, 255},
    {  0,  98, 255},
    {  0, 112, 255},
    {  0, 127, 255},
    {  0, 141, 255},
    {  0, 155, 255},
    {  0, 169, 255},
    {  0, 183, 255},
    {  0, 198, 255},
    {  0, 212, 255},
    {  0, 226, 255},
    {  0, 240, 255},
    {  0, 255, 255},
    { 14, 255, 240},
    { 28, 255, 226},
    { 42, 255, 212},
    { 56, 255, 198},
    { 70, 255, 183},
    { 84, 255, 169},
    { 98, 255, 155},
    {112, 255, 141},
    {127, 255, 127},
    {141, 255, 112},
    {155, 255,  98},
    {169, 255,  84},
    {183, 255,  70},
    {198, 255,  56},
    {212, 255,  42},
    {226, 255,  28},
    {240, 255,  14},
    {255, 255,   0},
    {255, 240,   0},
    {255, 226,   0},
    {255, 212,   0},
    {255, 198,   0},
    {255, 183,   0},
    {255, 169,   0},
    {255, 155,   0},
    {255, 141,   0},
    {255, 127,   0},
    {255, 112,   0},
    {255,  98,   0},
    {255,  84,   0},
    {255,  70,   0},
    {255,  56,   0},
    {255,  42,   0},
    {255,  28,   0},
    {255,  14,   0},
    {255,   0,   0},
    {240,   0,   0},
    {226,   0,   0},
    {212,   0,   0},
    {198,   0,   0},
    {183,   0,   0},
    {169,   0,   0},
    {155,   0,   0},
    {141,   0,   0}
};
