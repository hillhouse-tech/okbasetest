#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "hhmessageevent.h"
#include "frontpanel.h"
#include "hhsequencemgr.h"
#include "hhmessenger.h"
#include "hhconstants.h"
#include "hhdatareader.h"

#include <QDebug>
#include <QSlider>
#include <QPushButton>
#include <QScrollBar>
#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QTimer>
#include "hhsliderwidget.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mpDialog(nullptr),
    mpFrontPanel(nullptr),
    mpSeqMgr(nullptr),
    mDialogCol(0),
    mDialogRow(0),
    mpRecordFile(nullptr),
    mpRecordStream(nullptr),
    mIsRecording(false),
    mPipeoutAllowed(true)
{
    ui->setupUi(this);
    this->ui->messageBox->document()->setMaximumBlockCount(MAX_LOG_LINES);
    this->ui->messageBox->append("Welcome\n");

    // combo box
    QComboBox* cbImageSelect0 = this->ui->cbImageSelect0;
    cbImageSelect0->addItem("OFF", QVariant(-1));
    cbImageSelect0->addItem("Full Picture", QVariant(0));
    cbImageSelect0->addItem("Digital", QVariant(1));
    cbImageSelect0->addItem("Marked Full Picture", QVariant(2));
    cbImageSelect0->addItem("Optical Flow 0", QVariant(3));
    cbImageSelect0->addItem("Optical Flow 1", QVariant(4));
    cbImageSelect0->setCurrentIndex(1);
    QComboBox* cbImageSelect1 = this->ui->cbImageSelect1;
    cbImageSelect1->addItem("OFF", QVariant(-1));
    cbImageSelect1->addItem("Full Picture", QVariant(0));
    cbImageSelect1->addItem("Digital", QVariant(1));
    cbImageSelect1->addItem("Marked Full Picture", QVariant(2));
    cbImageSelect1->addItem("Optical Flow 0", QVariant(3));
    cbImageSelect1->addItem("Optical Flow 1", QVariant(4));
    cbImageSelect1->setCurrentIndex(2);

    connect(cbImageSelect0, SIGNAL(currentIndexChanged(int)), this, SLOT(handleCbSelect(int)));
    connect(cbImageSelect1, SIGNAL(currentIndexChanged(int)), this, SLOT(handleCbSelect(int)));

    // set up display widget
    connect(this, SIGNAL(updateMainDisplay(const QImage&)), this->ui->mainDisplay, SLOT(handleUpdateDisplay(const QImage&)));
    connect(this, SIGNAL(updateSecondDisplay(const QImage&)), this->ui->secondDisplay, SLOT(handleUpdateDisplay(const QImage&)));

    // settings dialog
    mpDialog = new QDialog(this);
    mpDialog->setFixedWidth(800);
    mpDialog->setLayout(new QGridLayout);

    // init FrontPanel
    FrontPanel* pFrontPanel = FrontPanel::getInstance();
    pFrontPanel->setWindow(this);

    setFrontPanel(pFrontPanel);


    // init messenger
    HHMessenger* pMessenger = HHMessenger::getInstance();
    pMessenger->setWindow(this);

    mpSeqMgr = new HHSequenceMgr;
    if (mpSeqMgr->initCommandList() && mpSeqMgr->initSequenceList())
    {
        pMessenger->sendMessage("Commands and Sequences ready.", EMessageType::success);

        // init ui
        QGridLayout* sequenceLayout = this->ui->sequenceGridLayout;

        QVector<QString> sequenceNames = mpSeqMgr->getAllSequenceNames();
        int row = 0;
        int col = 0;
        for (auto itr = sequenceNames.begin(); itr != sequenceNames.end(); itr++)
        {
            QString name = *itr;
            HHSequence* pSeq = mpSeqMgr->getSequenceByName(name);

            if (!pSeq)
                continue;

            // show or not
            if (!pSeq->isShown())
                continue;

            if (pSeq->isAdvanced())
            {
                addToDialog(name);
                continue;
            }

            // HHMessenger::getInstance()->sendMessage(*itr);
            QPushButton* pButton = new QPushButton(this);
            pButton->setText(*itr);
            connect(pButton, SIGNAL (released()), this, SLOT (handlePushButton()));
            sequenceLayout->addWidget(pButton, row, col);

            col++;
            if (col >= SEQUENCE_LAYOUT_WIDTH)
            {
                row++;
                col = 0;
            }
        }

        // init sliders
        if (mpSeqMgr->initSliderList())
        {
            QGridLayout* sliderLayout = this->ui->sliderGridLayout;
            QVector<QString> sliderNames = mpSeqMgr->getAllSliderNames();

            int row = 0;
            int col = 0;
            for (auto itr = sliderNames.begin(); itr != sliderNames.end(); itr++)
            {
                QString name = *itr;
                HHSequenceSlider* pSliderSeq = mpSeqMgr->getSliderByName(name);

                if (!pSliderSeq)
                    continue;

                // show or not
                if (!pSliderSeq->isShown())
                    continue;

                // advanced
                if (pSliderSeq->isAdvanced())
                {
                    mAdvancedNames.push_back(name);
                    addToDialog(name);
                    continue;
                }

                uint32_t max = pSliderSeq->getMax();
                uint32_t min = pSliderSeq->getMin();
                uint32_t initial = pSliderSeq->getValue();

                HHSliderWidget* pSlider = new HHSliderWidget(this, name, min, max, initial);

                sliderLayout->addWidget(pSlider, row, col);

                connect(pSlider, SIGNAL(valueChanged(uint32_t)), this, SLOT(handleSliderValueChanged(uint32_t)));

                // keep the initial values
                mSliderNameValueMap.insert(name, initial);

                col++;
                if (col >= SLIDER_LAYOUT_WIDTH)
                {
                    row++;
                    col = 0;
                }
            }
        }


        //
        // render thread
        //
        mpData = new unsigned char[BUFFER_SIZE];
        qRegisterMetaType<QImage>();
        if (!mRenderThread.setFpnFile("FPN.txt"))
            pMessenger->sendMessage("Can't load FPN.txt or the file is invalid", EMessageType::error);
        connect(&mRenderThread, SIGNAL(renderedImage(const QImage&, int)), this, SLOT(handleRenderedImage(const QImage&, int)), Qt::ConnectionType::QueuedConnection);
        connect(&mRenderThread, SIGNAL(commandCallback(QString)), this, SLOT(handleCommandCallback(QString)), Qt::ConnectionType::QueuedConnection);
        mRenderThread.start();

//        //
//        pFrontPanel->initializeFPGA();
//        //
//        powerUp();
//        //
//        loadInitialSettings();
        reinitializeFPGA(0);

        //
        // begin pipe out
        //
        // set up timer
        mpPipeOutTimer = new QTimer(this);
        mpPipeOutTimer->setSingleShot(true);
        connect(mpPipeOutTimer, SIGNAL(timeout()), this, SLOT(handlePipeoutTimer()));
        mpPipeOutTimer->start(PIPEOUT_TIMER);

    }
}

void MainWindow::handleRenderedImage(const QImage& image, int whichDisplay)
{
//    bool ok = image.save(QCoreApplication::applicationDirPath() + "/2.jpg");
//    if (!ok)
//    {

//    }
    if (whichDisplay == 0)
        emit updateMainDisplay(image);
    else if (whichDisplay == 1)
        emit updateSecondDisplay(image);
}

void MainWindow::handleCbSelect(int index)
{
    QComboBox* cb = dynamic_cast<QComboBox*>(sender());
    int value = cb->itemData(index).toInt();

    if (cb == this->ui->cbImageSelect0)
    {
        mRenderThread.setImageSelect(0, value);
    }
    else if (cb == this->ui->cbImageSelect1)
    {
        mRenderThread.setImageSelect(1, value);
    }
}

void MainWindow::handleCommandCallback(QString info)
{
    if (info == "up_brightness")
    {
        HHMessenger::getInstance()->sendMessage("Reducing brightness");
        setBrightness(mSliderNameValueMap["CDS_DC"] + 10);
    }
    else if (info == "down_brightness")
    {
        HHMessenger::getInstance()->sendMessage("Increasing brightness");
        setBrightness(mSliderNameValueMap["CDS_DC"] - 10);
    }
    else if (info == "fpn_done")
    {
        QMessageBox *dialog = new QMessageBox(this);
        dialog->setFixedWidth(300);
        dialog->setText("FPN generated");
        dialog->setWindowTitle("Message");
        dialog->show();
    }
}

void MainWindow::handlePipeoutTimer()
{
    if (!mPipeoutAllowed)
        return;

    uint32_t sdramFull, pageCount;
    mpFrontPanel->wireOut(0x20, 0x0001, &sdramFull);
    mpFrontPanel->wireOut(0x21, 0x1FFFFF, &pageCount);
    //HHMessenger::getInstance()->sendMessage("count " + QString::number(pageCount), EMessageType::action);

    if (sdramFull > 0)
    {
        HHMessenger::getInstance()->sendMessage("SDRAM is full. ", EMessageType::error);
        return;
    }

    if (pageCount >= 10)
    {
        long dataLen = mpFrontPanel->blockPipeOut(0xa0, 128, pageCount * 128, mpData);
        // HHMessenger::getInstance()->sendMessage("pipe out " + QString::number(dataLen), EMessageType::action);

        if (dataLen > 0)
        {
            if (mIsRecording && mpRecordStream)
            {
                mpRecordStream->writeRawData((char*)mpData, dataLen);
            }

            //HHMessenger::getInstance()->sendMessage("write out " + QString::number(n), EMessageType::action);
            unsigned char* dataIn = new unsigned char[dataLen];
            memcpy(dataIn, mpData, dataLen);
            mRenderThread.addData(dataIn, dataLen);
        }
    }

    mpPipeOutTimer->start(PIPEOUT_TIMER);
}

MainWindow::~MainWindow()
{
    if (mpRecordFile) mpRecordFile->close();
    mRenderThread.terminate();
    mpFrontPanel->uninitializeFPGA();
    delete mpSeqMgr;
    delete mpDialog;
    delete ui;
}

void MainWindow::powerUp()
{
    //
    // Execute Sensor Power-up Sequence
    //
    QString sPowerUp = "Power Up";
    if (HHSequence* pUpSeq = mpSeqMgr->getSequenceByName(sPowerUp))
    {
        if (!pUpSeq->fire())
        {
            HHMessenger::getInstance()->sendMessage("Power Up failed.", EMessageType::error);
        }
    }
    else
    {
        HHMessenger::getInstance()->sendMessage("\"" + sPowerUp + "\" Sequence not defined.", EMessageType::error);
    }
}

void MainWindow::loadInitialSettings()
{
    //
    // load initial settings
    //
    bool bOk = false;
    QString settingNames;
    if (mpFrontPanel->isReady())
    {
        bOk = true;

        for (auto itr = mAdvancedNames.begin(); itr != mAdvancedNames.end(); itr++)
        {
            if (HHSequenceSlider* pSeqSlider = mpSeqMgr->getSliderByName(*itr))
            {
                uint32_t arg = mSliderNameValueMap[*itr];
                bool ret = pSeqSlider->fireWithArg(arg);
                if (!ret)
                {
                    settingNames += (" " + (*itr));
                }
                bOk = (bOk && ret);
            }
        }

    }
    if (bOk)
    {
        HHMessenger::getInstance()->sendMessage("Settings loaded.", EMessageType::success);
    }
    else
    {
        HHMessenger::getInstance()->sendMessage("Settings not loaded: " +settingNames, EMessageType::error);
    }
}

void MainWindow::setFrontPanel(FrontPanel* pFrontPanel)
{
    mpFrontPanel = pFrontPanel;
}

void MainWindow::appendMessage(QString msg, EMessageType type)
{
    switch(type) {
        case EMessageType::info:
            this->ui->messageBox->insertHtml("<font color='black'>" + msg + "</font>");
            break;
        case EMessageType::success:
            this->ui->messageBox->insertHtml("<font color='limegreen'>" + msg + "</font>");
            break;
        case EMessageType::action:
            this->ui->messageBox->insertHtml("<font color='blue'>" + msg + "</font>");
            break;
        case EMessageType::error:
            this->ui->messageBox->insertHtml("<font color='red'>" + msg + "</font>");
            break;
        default:
            this->ui->messageBox->insertHtml("<font color='black'>" + msg + "</font>");
    }
    this->ui->messageBox->append(""); // create new block
    QTextCursor c = this->ui->messageBox->textCursor();
    c.movePosition(QTextCursor::End);
    this->ui->messageBox->setTextCursor(c);
}

bool MainWindow::event(QEvent *event)
{
    if(event->type() == HHMessageEvent::HHMessageEventType) {
        if (auto e = dynamic_cast<HHMessageEvent*>(event))
        {
            appendMessage(e->getMessage(), e->getType());
        }
        else
        {
            appendMessage("(Message Lost.)", EMessageType::error);
        }
        return true;
    }
    if(event->type() == QEvent::MouseButtonPress)
    {
        QCoreApplication::postEvent(this, new HHMessageEvent("Sent ok.", EMessageType::action));
    }

    return  QWidget::event(event);
}

void MainWindow::handlePushButton()
{
    QPushButton* pButton = qobject_cast<QPushButton*>(sender());
    QString name = pButton->text();

    // run the sequence
    HHSequence* pSeq = mpSeqMgr->getSequenceByName(name);
    bool bOk = false;

    if (pSeq)
    {
        bOk = pSeq->fire();
        QString next = pSeq->getNext();
        if (!next.isEmpty())
        {
            pButton->setText(next);
        }
    }

    if (bOk)
    {
        HHMessenger::getInstance()->sendMessage("Executed: " + name, EMessageType::action);
    }
    else
    {
        HHMessenger::getInstance()->sendMessage("Failed: " + name, EMessageType::error);
    }
}

void MainWindow::handleSliderValueChanged(uint32_t newValue)
{
    HHSliderWidget* pSlider = qobject_cast<HHSliderWidget*>(sender());
    QString name = pSlider->getName();

    bool bOk = false;
    HHSequenceSlider* pSeqSlider = mpSeqMgr->getSliderByName(name);
    if (pSeqSlider)
    {
        bOk = pSeqSlider->fireWithArg(newValue);
        if (bOk)
        {
            // update the map
            mSliderNameValueMap[name] = newValue;
        }
    }

    if (bOk)
    {
        HHMessenger::getInstance()->sendMessage("Executed: " + name, EMessageType::action);
    }
    else
    {
        HHMessenger::getInstance()->sendMessage("Failed: " + name, EMessageType::error);
    }
}

void MainWindow::showDialog()
{
    refreshDialog();

    mpDialog->show();
    mpDialog->raise();
    mpDialog->activateWindow();
}

void MainWindow::setThreshold(uint32_t value)
{
    unsigned EVT_DC = mSliderNameValueMap["EVT_DC"];
    uint32_t EVT_VL = EVT_DC - value;
    uint32_t EVT_VH = EVT_DC + value;
    HHSequenceSlider* pSlider = mpSeqMgr->getSliderByName("EVT_VL");
    pSlider->fireWithArg(EVT_VL);
    pSlider = mpSeqMgr->getSliderByName("EVT_VH");
    pSlider->fireWithArg(EVT_VH);
    mSliderNameValueMap["EVT_VL"] = EVT_VL;
    mSliderNameValueMap["EVT_VH"] = EVT_VH;
}

void MainWindow::setBrightness(uint32_t value)
{
    HHSequenceSlider* pSlider = mpSeqMgr->getSliderByName("CDS_DC");
    pSlider->fireWithArg(value);
    mSliderNameValueMap["CDS_DC"] = value;
}

void MainWindow::setContrast(uint32_t value)
{
    uint32_t REF_PLUS = 512 + value / 2;
    uint32_t REF_MINUS = 512 - value / 2;
    uint32_t REF_PLUS_H = REF_PLUS + value / 16;
    uint32_t REF_MINUS_H = REF_MINUS - value / 16;
    HHSequenceSlider* pSlider = mpSeqMgr->getSliderByName("REF+");
    pSlider->fireWithArg(REF_PLUS);
    pSlider = mpSeqMgr->getSliderByName("REF-");
    pSlider->fireWithArg(REF_MINUS);
    pSlider = mpSeqMgr->getSliderByName("REF+H");
    pSlider->fireWithArg(REF_PLUS_H);
    pSlider = mpSeqMgr->getSliderByName("REF-H");
    pSlider->fireWithArg(REF_MINUS_H);
    mSliderNameValueMap["REF+"] = REF_PLUS;
    mSliderNameValueMap["REF-"] = REF_MINUS;
    mSliderNameValueMap["REF+H"] = REF_PLUS_H;
    mSliderNameValueMap["REF-H"] = REF_MINUS_H;
}

void MainWindow::setSample(uint32_t value)
{
    mRenderThread.setSample(value);
}

void MainWindow::setRecording(bool bStart)
{
    if (bStart)
    {
        const QDateTime now = QDateTime::currentDateTime();
        const QString timestamp = now.toString(QLatin1String("yyyyMMdd-hhmmsszzz"));

        mpRecordFile = new QFile(QCoreApplication::applicationDirPath() + "/Recording_" + timestamp + ".bin");
        bool bOpen = mpRecordFile->open(QIODevice::WriteOnly);
        if (!bOpen) HHMessenger::getInstance()->sendMessage("Can't open recording file.", EMessageType::error);
        mpRecordStream = new QDataStream(mpRecordFile);
        // write a header
        char header[8];
        int value = 27000;
        header[0] =      value & 0x000000FF;
        header[1] = (value>>8) & 0x000000FF;
        header[2] = (value>>16) & 0x000000FF;
        header[3] = (value>>24) & 0x000000FF;
        header[4] = 0;
        header[5] = 0;
        header[6] = 0;
        header[7] = mMode; // !
        mpRecordStream->writeRawData(header, 8);

        mIsRecording = true;
    }
    else
    {
        mIsRecording = false;

        mpRecordFile->close();
        delete mpRecordStream; mpRecordStream = nullptr;
        delete mpRecordFile; mpRecordFile = nullptr;
    }
}

void MainWindow::playback()
{
    if (mPlaybackStream.is_open())
        mPlaybackStream.close();

    // stop pipe out
    mPipeoutAllowed = false;

    QString filePath = QFileDialog::getOpenFileName(this, "Open a bin file", QCoreApplication::applicationDirPath(), "Bin Files (*.bin)");
    if (filePath.isEmpty())
        return;

    mPlaybackStream.open(filePath.toStdString(), std::ios::binary);
    if (!mPlaybackStream.good())
    {
        HHMessenger::getInstance()->sendMessage("Can't Open File: " + filePath, EMessageType::error);
        return;
    }

    HHMessenger::getInstance()->sendMessage("Playback File: " + filePath);

    // read header
    char header[8];
    mPlaybackStream.read(header, 8);
    mMode = header[7];
    mRenderThread.setMode(mMode);
    mRenderThread.setIsPlayback(true);
    HHMessenger::getInstance()->sendMessage("Playback Mode: " + QString::number(mMode), EMessageType::action);

    // [TODO]
    if (mMode == 0)
    {
        HHMessenger::getInstance()->sendMessage("Playback only support forcefire mode now.", EMessageType::error);
        return;
    }

    // start timer
    mpPlaybackTimer = new QTimer(this);
    mpPlaybackTimer->setSingleShot(true);
    connect(mpPlaybackTimer, SIGNAL(timeout()), this, SLOT(handlePlaybackTimer()));
    mpPlaybackTimer->start(20);
}

void MainWindow::handlePlaybackTimer()
{
    bool eof = false;
    int lenToRead = 4;
    if (mMode > 0)
        lenToRead = 1968644;

    char* data = new char[lenToRead];
    int dataLen = 0;
    int seCount = 0;
    while (true && mRenderThread.queueSize() < 1000000000)
    {
        qApp->processEvents();
        mPlaybackStream.read((char*)data, lenToRead);

        if (!mPlaybackStream.eof())
        {
            dataLen = lenToRead;
            unsigned char* dataIn = new unsigned char[dataLen];
            memcpy(dataIn, data, dataLen);
            mRenderThread.addData((unsigned char*)dataIn, dataLen);
            if (lenToRead == 4 && HHDataReader::isSpecialEvent(dataIn))
            {
                if (++seCount > SE_TILL_RENDER)
                    break;
            }
        }
        else
        {
            HHMessenger::getInstance()->sendMessage("Playback Finished.", EMessageType::action);
            eof = true;
            break;
        }
    }

    if (!eof)
    {
        mpPlaybackTimer->start(20);
    }

    delete [] data;
}

void MainWindow::reinitializeFPGA(uint32_t value)
{
    if (value == 0)
    {
        mpFrontPanel->initializeFPGA();
    }
    else
    {
        setThreshold(100);
        QString bitfileName = "top" + QString::number(value) + ".bit";
        mpFrontPanel->initializeFPGA(bitfileName);
    }

    mRenderThread.setMode(value);
    mMode = value; // set mode for recording file
    powerUp();
    loadInitialSettings();
}

void MainWindow::generateFPN(uint32_t value)
{
    Q_UNUSED(value);
    mRenderThread.generateFPN();
}

void MainWindow::adjustBrightness(uint32_t value)
{
    Q_UNUSED(value);
    mRenderThread.adjustBrightness();
}

void MainWindow::setUpperADC(uint32_t value)
{
    mRenderThread.setUpperADC(value);
}

void MainWindow::setLowerADC(uint32_t value)
{
    mRenderThread.setLowerADC(value);
}

void MainWindow::addToDialog(const QString& name)
{

    if (HHSequenceSlider* pSliderSeq = mpSeqMgr->getSliderByName(name))
    {
        uint32_t max = pSliderSeq->getMax();
        uint32_t min = pSliderSeq->getMin();
        uint32_t initial = pSliderSeq->getValue();

        HHSliderWidget* pSlider = new HHSliderWidget(mpDialog, name, min, max, initial);
        dynamic_cast<QGridLayout*>(mpDialog->layout())->addWidget(pSlider, mDialogRow, mDialogCol);
        connect(pSlider, SIGNAL(valueChanged(uint32_t)), this, SLOT(handleSliderValueChanged(uint32_t)));

        // keep the initial values
        mSliderNameValueMap.insert(name, initial);
    }
    else if (HHSequence* pSeq = mpSeqMgr->getSequenceByName(name))
    {
        QPushButton* pButton = new QPushButton(this);
        pButton->setText(name);
        connect(pButton, SIGNAL (released()), this, SLOT (handlePushButton()));
        dynamic_cast<QGridLayout*>(mpDialog->layout())->addWidget(pButton, mDialogRow, mDialogCol);
    }
    else
    {
        HHMessenger::getInstance()->sendMessage("Invalid advanced control: " + name, EMessageType::error);
        return;
    }

    mDialogCol++;
    if (mDialogCol >= DIALOG_LAYOUT_WIDTH)
    {
        mDialogRow++;
        mDialogCol = 0;
    }
}

void MainWindow::refreshDialog()
{
    for (int i = 0; i < mpDialog->layout()->count(); ++i)
    {
        QWidget* w = mpDialog->layout()->itemAt(i)->widget();
        if (HHSliderWidget* s = dynamic_cast<HHSliderWidget*>(w))
        {
            s->setValue(mSliderNameValueMap[s->getName()]);
        }
    }
}
