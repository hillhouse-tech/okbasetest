#ifndef HHMESSAGEEVENT_H
#define HHMESSAGEEVENT_H


#include <QEvent>
#include <QString>

enum class EMessageType
{
    info = 0,
    success,
    action,
    error
};

class HHMessageEvent : public QEvent {
public:
    static QEvent::Type HHMessageEventType;

    HHMessageEvent(QString message, EMessageType messageType= EMessageType::info);
    virtual ~HHMessageEvent();
    QString getMessage();
    EMessageType getType();

private:
    static QEvent::Type type()
    {
        if (HHMessageEvent::HHMessageEventType == QEvent::None)
        {
            int generatedType = QEvent::registerEventType();
            HHMessageEvent::HHMessageEventType = static_cast<QEvent::Type>(generatedType);
        }
        return HHMessageEvent::HHMessageEventType;
    }

private:
    QString mMessage;
    EMessageType mMessageType;
};
#endif // HHMESSAGEEVENT_H
