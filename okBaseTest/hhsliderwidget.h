#ifndef HHSLIDERCONTROL_H
#define HHSLIDERCONTROL_H

#include <stdint.h>
#include <QWidget>
#include <QLabel>
#include <QSlider>

class HHSliderWidget : public QWidget
{
    Q_OBJECT
public:
    explicit HHSliderWidget(QWidget *parent, QString name, uint32_t min, uint32_t max, uint32_t value);

    uint32_t getValue();
    QString getName();
signals:
    void valueChanged(uint32_t newValue);

public slots:
    void setValue(int newValue);
    void unblockSliderSignal();

private:
    QLabel* mpName;
    QLabel* mpMin;
    QSlider* mpSlider;
    QLabel* mpMax;
    QLabel* mpValue;

    uint32_t mValue;
};

#endif // HHSLIDERCONTROL_H
