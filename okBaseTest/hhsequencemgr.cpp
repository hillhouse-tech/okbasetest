#include "hhsequencemgr.h"
#include <qdebug.h>
#include <QDomDocument>
#include "hhcommand.h"
#include "hhxmlreader.h"
#include "hhconstants.h"
#include "hhmessenger.h"

HHSequenceMgr::HHSequenceMgr()
{
}

HHSequenceMgr::~HHSequenceMgr()
{
    while(mCommandList.size() > 0)
    {
        HHCommandBase* cmd = mCommandList.back();
        mCommandList.pop_back();
        delete cmd;
    }
    while(mSequenceList.size() > 0)
    {
        HHSequence* seq = mSequenceList.back();
        mSequenceList.pop_back();
        delete seq;
    }
}

bool HHSequenceMgr::initCommandList()
{
    HHXmlReader xml;
    QDomDocument dom;
    if (xml.parse(FILE_COMMANDS, &dom))
    {
        if (xml.importCommands(mCommandList, &dom))
        {
            for(auto itr = mCommandList.begin(); itr != mCommandList.end(); itr++)
            {
                if (!mCommandMap.contains((*itr)->name()))
                {
                    mCommandMap.insert((*itr)->name(), *itr);
                }
                else
                {
                    HHMessenger::getInstance()->sendMessage("More than one command has the same name:" + (*itr)->name(), EMessageType::error);
                    qDebug() << "More than one command has the same name:" << (*itr)->name();
                }
            }
            return true;
        }
    }
    return false;
}

bool HHSequenceMgr::initSequenceList()
{
    HHXmlReader xml;
    QDomDocument dom;
    if (xml.parse(FILE_SEQUENCES, &dom))
    {
        if (xml.importSequences(this, mSequenceList, &dom))
        {
            for (auto itr = mSequenceList.begin(); itr != mSequenceList.end(); itr++)
            {
                if (!mSequenceMap.contains((*itr)->name()))
                {
                    mSequenceMap.insert((*itr)->name(), *itr);
                }
                else
                {
                    HHMessenger::getInstance()->sendMessage("More than one sequence has the same name:" + (*itr)->name(), EMessageType::error);
                    qDebug() << "More than one sequence has the same name:" << (*itr)->name();
                }
            }
            return true;
        }
    }
    return false;
}

bool HHSequenceMgr::initSliderList()
{
    HHXmlReader xml;
    QDomDocument dom;
    if (xml.parse(FILE_SLIDERS, &dom))
    {
        if (xml.importSliders(this, mSliderList, &dom))
        {
            for (auto itr = mSliderList.begin(); itr != mSliderList.end(); itr++)
            {
                if (!mSliderMap.contains((*itr)->name()))
                {
                    mSliderMap.insert((*itr)->name(), *itr);
                }
                else
                {
                    HHMessenger::getInstance()->sendMessage("More than one slider has the same name:" + (*itr)->name(), EMessageType::error);
                    qDebug() << "More than one slider has the same name:" << (*itr)->name();
                }
            }
            return true;
        }
    }
    return false;
}

HHCommandBase* HHSequenceMgr::getCommandByName(const QString& name)
{
    if (mCommandMap.contains(name))
    {
        return mCommandMap[name];
    }

    return nullptr;
}

HHSequence* HHSequenceMgr::getSequenceByName(const QString& name)
{
    if (mSequenceMap.contains(name))
    {
        return mSequenceMap[name];
    }
    return nullptr;
}

HHSequenceSlider* HHSequenceMgr::getSliderByName(const QString& name)
{
    if (mSliderMap.contains(name))
    {
        return dynamic_cast<HHSequenceSlider*>(mSliderMap[name]);
    }
    return nullptr;
}

QVector<QString> HHSequenceMgr::getAllSequenceNames()
{
    QVector<QString> names;
    for (auto itr = mSequenceList.begin(); itr != mSequenceList.end(); itr++)
    {
        names.push_back((*itr)->name());
    }
    return names;
}

QVector<QString> HHSequenceMgr::getAllSliderNames()
{
    QVector<QString> names;
    for (auto itr = mSliderList.begin(); itr != mSliderList.end(); itr++)
    {
        names.push_back((*itr)->name());
    }
    return names;
}

//
// =========================================
//

HHSequence::HHSequence(const QString& name)
    :mName(name), mNext(""), mShow(true), mAdvanced(false)
{
}

HHSequence::~HHSequence()
{
}

QString HHSequence::name()
{
    return mName;
}

bool HHSequence::isShown()
{
    return mShow;
}

void HHSequence::setShow(bool bShow)
{
    mShow = bShow;
}

bool HHSequence::isAdvanced()
{
    return mAdvanced;
}

void HHSequence::setAdvanced(bool bAdvanced)
{
    mAdvanced = bAdvanced;
}

void HHSequence::addCommand(HHCommandBase* pCmd)
{
    mCommands.push_back(pCmd);
}

bool HHSequence::fire()
{
    for(auto itr = mCommands.begin(); itr != mCommands.end(); itr++)
    {
        if (!(*itr)->valid())
        {
            return false;
        }
    }
    for(auto itr = mCommands.begin(); itr != mCommands.end(); itr++)
    {
        (*itr)->execute();
    }
    return true;
}

QString HHSequence::getNext()
{
    return mNext;
}

void HHSequence::setNext(const QString& nextName)
{
    mNext = nextName;
}

//
// =========================================
//

HHSequenceSlider::HHSequenceSlider(const QString& name, uint32_t min, uint32_t max)
    :HHSequence(name), mMin(min), mMax(max)
{
}

bool HHSequenceSlider::fireWithArg(uint32_t newValue)
{
    for(auto itr = mCommands.begin(); itr != mCommands.end(); itr++)
    {
        if (!(*itr)->valid())
        {
            return false;
        }
    }
    for(auto itr = mCommands.begin(); itr != mCommands.end(); itr++)
    {
        if ((*itr)->needsArg())
            (*itr)->setValue(newValue);
        (*itr)->execute();
    }
    return true;
}

uint32_t HHSequenceSlider::getMax()
{
    return mMax;
}

uint32_t HHSequenceSlider::getMin()
{
    return mMin;
}

uint32_t HHSequenceSlider::getValue()
{
    return mValue;
}

void HHSequenceSlider::setValue(uint32_t value)
{
    mValue = value;
}
