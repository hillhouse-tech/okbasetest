#include "hhdatareader.h"
#include <assert.h>
#include <QtGlobal>

unsigned int HHDataReader::getCol(unsigned char data[EVENT_SIZE])
{
    unsigned int last7bit = 0;
    last7bit += static_cast<unsigned char>(data[0]) & 0x7F;
    unsigned int result = last7bit + ((static_cast<unsigned char>(data[1]) & 0x30) << 3);
    if ((data[1] & 0x40) != 0)
        result = PIXELS_PER_COL - 1 - result;
    return result;
}

unsigned int HHDataReader::getRow(unsigned char data[EVENT_SIZE])
{
    unsigned int last7bit = 0;
    last7bit += static_cast<unsigned char>(data[0]) & 0x7F;
    return last7bit + ((static_cast<unsigned char>(data[1]) & 0x70) << 3);
}

unsigned int HHDataReader::getTimeStamp(unsigned char data[EVENT_SIZE])
{
    unsigned int last4bit = 0;
    last4bit += static_cast<unsigned int>(data[1] & 0x0F);
    unsigned int mid7bit = 0;
    mid7bit += static_cast<unsigned int>(data[2] & 0x7F);
    return last4bit + (mid7bit << 4) + (static_cast<unsigned int>(data[3] & 0x7F) << 11);
}

unsigned int HHDataReader::getADC(unsigned char data[EVENT_SIZE])
{
    unsigned int last4bit = 0;
    last4bit += static_cast<unsigned int>(data[1] & 0x0F);
    return last4bit + (static_cast<unsigned int>(data[2] & 0x1F) << 4);
}

/* ----------------------------------------------------------------------- *
** -------------------- For Event-only Mode ------------------------------ *
** ----------------------------------------------------------------------- */
unsigned int HHDataReader::getSensorTime(unsigned char data[EVENT_SIZE])
{
    unsigned int last4bit = 0;
    last4bit += static_cast<unsigned int>(data[1] & 0x0F);
    return last4bit + (static_cast<unsigned int>(data[2] & 0x1F) << 4);
}

unsigned int HHDataReader::getPixelTag(unsigned char data[EVENT_SIZE])
{
    return data[3] & 0x01;
}

unsigned int HHDataReader::getSpecialEventTag(unsigned char data[EVENT_SIZE])
{
    return data[3] & 0x01;
}

unsigned int HHDataReader::getColTag(unsigned char data[EVENT_SIZE])
{
    return data[3] & 0x01;
}
/* ----------------------------------------------------------------------- *
** ----------------------------------------------------------------------- */

bool HHDataReader::isForcefirePixel(unsigned char data[EVENT_SIZE])	// column event from force fire, carrying ADC
{
    return ((unsigned char)data[3] & 0x01) == 1;
}
bool HHDataReader::isCol(unsigned char data[EVENT_SIZE])
{
    return ((unsigned char)data[0] & 0x80) == 0;
}
bool HHDataReader::isRow(unsigned char data[EVENT_SIZE])
{
    return ((unsigned char)data[0] & 0x80) > 0 && !isSpecialEvent(data);
}
bool HHDataReader::isSpecialEvent(unsigned char data[EVENT_SIZE])
{
    unsigned char a = (unsigned char)data[0];
    unsigned char b = (unsigned char)data[1];
    unsigned char c = (unsigned char)data[2];
    if (a == 0xff && b == 0xff && c == 0xff)
    {
        return true;
    }
    else
    {
        return false;
    }

}

unsigned int HHDataReader::sCurRow = PIXELS_PER_COL;
unsigned int HHDataReader::sCurT = 0;
