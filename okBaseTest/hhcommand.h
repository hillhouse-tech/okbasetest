#ifndef HHCOMMAND_H
#define HHCOMMAND_H

#include <QString>
#include <stdint.h>

class FrontPanel;

class HHCommandBase
{
public:
    HHCommandBase(const QString& name);
    ~HHCommandBase();

    QString name();

    virtual void execute() = 0;
    virtual bool valid();
    virtual void valid(bool bValid);
    virtual QString error();
    virtual void error(const QString& error);

    virtual void needsArg(bool bNeed);
    virtual bool needsArg();

    virtual void setValue(uint32_t value) = 0;

    virtual HHCommandBase* clone();

protected:
    QString mName;

private:
    bool mValid;
    bool mNeedsArg;
    QString mErrorMessage;
};

#endif // HHCOMMAND_H
