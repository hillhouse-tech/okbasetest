#include "hhsystemcommand.h"
#include "hhmessenger.h"
#include <QDialog>
#include "frontpanel.h"
#include "mainwindow.h"

HHSystemCommand::HHSystemCommand(const QString& name)
    :HHCommandBase(name)
{

}

void HHSystemCommand::setValue(uint32_t value)
{
    mArg = value;
}

void HHSystemCommand::execute()
{
    if(mName == "set lower ADC to #value#")
        SYSTEM_setLowerADC(mArg);
    else if (mName == "set upper ADC to #value#")
        SYSTEM_setUpperADC(mArg);
    else if (mName == "set brightness to #value#")
        SYSTEM_setBrightness(mArg);
    else if (mName == "set threshold to #value#")
        SYSTEM_setThreshold(mArg);
    else if (mName == "set contrast to #value#")
        SYSTEM_setContrast(mArg);
    else if (mName == "set sample to #value#")
        SYSTEM_setSample(mArg);
    else if (mName == "Start Recording")
        SYSTEM_startRecording(mArg);
    else if (mName == "Stop Recording")
        SYSTEM_stopRecording(mArg);
    else if (mName == "Playback")
        SYSTEM_playback(mArg);
    else if (mName == "Advanced Settings")
        SYSTEM_advancedSettings(mArg);
    else if (mName == "Reinitialize FPGA")
        SYSTEM_reinitializeFPGA(mArg);
    else if (mName == "Generate FPN")
        SYSTEM_generateFPN(mArg);
    else if (mName == "Adjust Brightness")
        SYSTEM_adjustBrightness(mArg);
    else
        HHMessenger::getInstance()->sendMessage("Unknow system command:" + mName, EMessageType::error);

}

void HHSystemCommand::SYSTEM_advancedSettings(uint32_t value)
{
    Q_UNUSED(value);
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->showDialog();
    }
}

void HHSystemCommand::SYSTEM_setLowerADC(uint32_t value)
{
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->setLowerADC(value);
    }
}

void HHSystemCommand::SYSTEM_setUpperADC(uint32_t value)
{
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->setUpperADC(value);
    }
}

void HHSystemCommand::SYSTEM_startRecording(uint32_t value)
{
    Q_UNUSED(value);
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->setRecording(true);
    }
}

void HHSystemCommand::SYSTEM_stopRecording(uint32_t value)
{
    Q_UNUSED(value);
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->setRecording(false);
    }
}

void HHSystemCommand::SYSTEM_playback(uint32_t value)
{
    Q_UNUSED(value);
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->playback();
    }
}

void HHSystemCommand::SYSTEM_setBrightness(uint32_t value)
{
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->setBrightness(value);
    }
}

void HHSystemCommand::SYSTEM_setThreshold(uint32_t value)
{
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->setThreshold(value);
    }
}

void HHSystemCommand::SYSTEM_setContrast(uint32_t value)
{
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->setContrast(value);
    }
}

void HHSystemCommand::SYSTEM_setSample(uint32_t value)
{
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->setSample(value);
    }
}

void HHSystemCommand::SYSTEM_reinitializeFPGA(uint32_t value)
{
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->reinitializeFPGA(value);
    }
}

void HHSystemCommand::SYSTEM_generateFPN(uint32_t value)
{
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->generateFPN(value);
    }
}

void HHSystemCommand::SYSTEM_adjustBrightness(uint32_t value)
{
    if (MainWindow* pWindow = dynamic_cast<MainWindow*>(FrontPanel::getInstance()->getWindow()))
    {
        pWindow->adjustBrightness(value);
    }
}
