#include "hhmessageevent.h"

HHMessageEvent::HHMessageEvent(QString message, EMessageType messageType)
    : QEvent(type()), mMessage(message), mMessageType(messageType)
{
}

HHMessageEvent::~HHMessageEvent()
{
}

QString HHMessageEvent::getMessage()
{
    return mMessage;
}

EMessageType HHMessageEvent::getType()
{
    return mMessageType;
}

QEvent::Type HHMessageEvent::HHMessageEventType = QEvent::None;
