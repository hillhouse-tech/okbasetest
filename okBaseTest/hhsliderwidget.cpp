#include "hhsliderwidget.h"
#include "hhconstants.h"
#include <QTimer>
#include <QGridLayout>

HHSliderWidget::HHSliderWidget(QWidget *parent, QString name, uint32_t min, uint32_t max, uint32_t value)
    : QWidget(parent), mValue(value)
{
    mpName = new QLabel(this);
    mpName->setText(name);

    mpMin = new QLabel(this);
    mpMin->setText(QString::number(min));

    mpSlider = new QSlider(Qt::Horizontal, this);
    mpSlider->setMinimum(min); mpSlider->setMaximum(max); mpSlider->setValue(value);

    mpMax = new QLabel(this);
    mpMax->setText(QString::number(max));

    mpValue = new QLabel(this);
    mpValue->setText(QString::number(value));
    QFont f( "Arial", 10, QFont::Bold);
    mpValue->setFont(f);

    QGridLayout* pLayout = new QGridLayout;
    setLayout(pLayout);

    pLayout->addWidget(mpName, 0, 0);
    pLayout->addWidget(mpMin, 0, 1);
    pLayout->addWidget(mpSlider, 0, 2);
    pLayout->addWidget(mpMax, 0, 3);
    pLayout->addWidget(mpValue, 0, 4);

    connect(mpSlider, SIGNAL(valueChanged(int)), this, SLOT(setValue(int)));
}

uint32_t HHSliderWidget::getValue()
{
    return mValue;
}

QString HHSliderWidget::getName()
{
    return mpName->text();
}

void HHSliderWidget::setValue(int newValue)
{
    if (newValue == mValue)
        return;

    mpSlider->blockSignals(true);
    QTimer::singleShot(SLIDER_DELAY, this, SLOT(unblockSliderSignal()));

    mpValue->setText(QString::number(newValue));
    mValue = newValue;

    // signal
    emit valueChanged(mValue);
}

void HHSliderWidget::unblockSliderSignal()
{
    mpSlider->blockSignals(false);
}
