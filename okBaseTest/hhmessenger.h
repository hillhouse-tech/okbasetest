#ifndef HHMESSENGER_H
#define HHMESSENGER_H


#include <QString>
#include "hhmessageevent.h"

class QWidget;

class HHMessenger
{
private:
    HHMessenger();

public:
    static HHMessenger* getInstance();
    void sendMessage(QString msg, EMessageType type = EMessageType::info);
    void setWindow(QWidget* pWin);

private:
    static HHMessenger* spInstance;
    QWidget* mpWindow;
};

#endif // HHMESSENGER_H
