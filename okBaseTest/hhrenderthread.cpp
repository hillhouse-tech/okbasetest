#include "hhrenderthread.h"
#include "hhmessenger.h"
#include <QCoreApplication>
#include <QDebug>

HHRenderThread::HHRenderThread(QObject* parent)
    :QThread(parent), mAbort(false), mIsPlayback(false), mMode(0), mPopCount(0), mSample(0)
{
    connect(this, SIGNAL(finished()), this, SLOT(onFinished()));
    connect(&mDataProcessor, SIGNAL(imageReady(int)), this, SLOT(handleImageReady(int)));
    connect(&mDataProcessor, SIGNAL(commandCallback(QString)), this, SLOT(handleCommandCallback(QString)));
}

HHRenderThread::~HHRenderThread()
{
    mMutex.lock();
    mAbort = true;
    mMutex.unlock();

    wait();
}

void HHRenderThread::onFinished()
{

}

void HHRenderThread::handleImageReady(int whichDisplay)
{
    // signal UI thread
    if (whichDisplay == 0)
    {
        QImage image = mDataProcessor.getImage0();
        emit renderedImage(image, whichDisplay);
    }
    else if (whichDisplay == 1)
    {
        QImage image = mDataProcessor.getImage1();
        emit renderedImage(image, whichDisplay);
    }
}

void HHRenderThread::handleCommandCallback(QString info)
{
    commandCallback(info);
}

void HHRenderThread::addData(unsigned char *data, long length)
{
    mDataQueue.push(data, length);
}

bool HHRenderThread::setFpnFile(const QString &fpnFile)
{
    return mDataProcessor.setFpnFile(fpnFile);
}

void HHRenderThread::setMode(int mode)
{
    mDataQueue.clear();
    mMode = mode;
    mDataProcessor.setMode(mode);
}

void HHRenderThread::setImageSelect(int whichImage, int select)
{
    mDataProcessor.setImageSelect(whichImage, select);
}

void HHRenderThread::setIsPlayback(bool bIsPlayback)
{
    mIsPlayback = bIsPlayback;
}

void HHRenderThread::generateFPN()
{
    mDataProcessor.generateFPN();
}

void HHRenderThread::adjustBrightness()
{
    mDataProcessor.adjustBrightness();
}

void HHRenderThread::setUpperADC(uint32_t value)
{
    mDataProcessor.setUpperADC(value);
}

void HHRenderThread::setLowerADC(uint32_t value)
{
    mDataProcessor.setLowerADC(value);
}

void HHRenderThread::setSample(uint32_t value)
{
    mSample = value;
}

uint32_t HHRenderThread::queueSize()
{
    return mDataQueue.size();
}

void HHRenderThread::run()
{
    forever
    {
        unsigned char* data = nullptr;
        long dataLen;
        mDataQueue.pop(data, &dataLen);

        if (dataLen > 0)
        {
            //HHMessenger::getInstance()->sendMessage("len = " + QString::number(dataLen), EMessageType::error);
            //mpThreadRecordStream->writeRawData((char*)data, dataLen);

            ++mPopCount;

            if (dataLen == 4)
                mDataProcessor.processPlaybackData(data, dataLen);
            else
            {
                if (mMode > 0)
                {
                    if (mPopCount >= mSample)
                    {
                        mDataProcessor.processData(data, dataLen);
                        mPopCount = 0;
                    }
                }
                else
                {
                    mDataProcessor.processData(data, dataLen);
                }
            }
        }

        delete [] data;
    }
}



