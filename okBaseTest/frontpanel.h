#ifndef FRONTPANEL_H
#define FRONTPANEL_H

#include "okFrontPanelDLL.h"
#include <QMainWindow>
#include "hhmessageevent.h"
#include <QCoreApplication>
#include <QFile>
#include <QTime>

class FrontPanel
{
private:
    FrontPanel()
        :mReady(false)
    {
        myxem = new okCFrontPanel;              // xem
        mypll = new okCPLL22393;                // pll22393
    }


public:
    static FrontPanel* getInstance()
    {
        if (!spFrontPanel)
        {
            spFrontPanel = new FrontPanel;
        }
        return spFrontPanel;
    }

    void setWindow(QWidget* pWindow)
    {
        mWindow = pWindow;
    }

    QWidget* getWindow()
    {
        return mWindow;
    }

    void initializeFPGA(const QString& bitfileName = "top.bit")
    {
        sendMessage("Begin initialize FPGA.");

        if (!myxem->IsOpen())
        {
            if (okCFrontPanel::NoError != myxem->OpenBySerial())
            {
                sendMessage("FrontPanel Device not found..", EMessageType::error);
                return;
            }
        }

        std::string modelString = myxem->GetBoardModelString(myxem->GetBoardModel()); //first device model type
        sendMessage(QString::fromStdString("Device type: " + modelString));

        // Configure the PLL appropriately
        myxem->LoadDefaultPLLConfiguration();

        sendMessage(QString::fromStdString("Device serial number: " + myxem->GetSerialNumber()));
        sendMessage(QString::fromStdString("Device ID string: " + myxem->GetDeviceID()));

        if (okCFrontPanel::NoError != myxem->OpenBySerial(myxem->GetSerialNumber()))
        {
            sendMessage("Can't open Serial.", EMessageType::error);
            return;
        }
        else
        {
            sendMessage(("Serial opens successfully."));
        }

        QString bitfilePath = QCoreApplication::applicationDirPath() + "/" + bitfileName;

        QFile bitfile(bitfilePath);
        if (!bitfile.exists())
        {
            sendMessage("Can't find " + bitfileName + " in the directory.", EMessageType::error);
            return;
        }

        std::string s = bitfilePath.toStdString();

        if (okCFrontPanel::NoError != myxem->ConfigureFPGA(bitfilePath.toStdString()))
        {
            sendMessage("Fail to load *.bit file!", EMessageType::error);
            return;
        }

        mReady = true;

        sendMessage("FPGA Ready.", EMessageType::success);

        //operation_init_sensor_config();

        //operation_reset_dereset_ALL();
    }

    void uninitializeFPGA()
    {
        myxem->ResetFPGA();
        myxem->Close();
    }

    bool isReady()
    {
        return mReady;
    }

    //
    // ============= Basic Operations ===============
    //
    bool wireIn(uint32_t address, uint32_t value, uint32_t mask)
    {
        if (okCFrontPanel::NoError != myxem->SetWireInValue(address, value, mask))
        {
            sendMessage("WireIn error", EMessageType::error);
            return false;
        }
        myxem->UpdateWireIns();
        return true;
    }

    void wireOut(uint32_t address, uint32_t mask, uint32_t* pValue)
    {
        myxem->UpdateWireOuts();
        *pValue = (myxem->GetWireOutValue(address)) & mask;
    }

    long blockPipeOut(uint32_t address, int blockSize, long length, unsigned char *data)
    {
        long dataLen = myxem->ReadFromBlockPipeOut(address, blockSize, length, data); //format is addr: blockSize: byte number: buffer
        return dataLen;
    }

    void wait(int ms)
    {
        QTime dieTime= QTime::currentTime().addMSecs(ms);
        while (QTime::currentTime() < dieTime)
            QCoreApplication::processEvents(QEventLoop::AllEvents, 10);
    }

    //
    // ====================================================
    //

private:
    void sendMessage(QString msg, EMessageType type = EMessageType::info)
    {
        QCoreApplication::postEvent(mWindow, new HHMessageEvent(msg, type));
    }

public:

private:
    bool mReady;
    static FrontPanel* spFrontPanel;
    QWidget* mWindow;
    okCFrontPanel* myxem;
    okCPLL22393* mypll;
};

#endif // FRONTPANEL_H
