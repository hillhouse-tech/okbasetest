#-------------------------------------------------
#
# Project created by QtCreator 2017-03-06T20:58:14
#
#-------------------------------------------------

CONFIG += qt thread
QT       += core gui
QT       += xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = okBaseTest
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    hhmessageevent.cpp \
    hhdisplaywidget.cpp \
    hhxmlreader.cpp \
    hhcommand.cpp \
    hhwireincommand.cpp \
    hhdelaycommand.cpp \
    frontpanel.cpp \
    hhsequencemgr.cpp \
    hhmessenger.cpp \
    hhsliderwidget.cpp \
    hhsystemcommand.cpp \
    hhrenderthread.cpp \
    hhdataqueue.cpp \
    hhdatareader.cpp \
    hhdataprocessor.cpp

HEADERS  += mainwindow.h \
    okFrontPanelDLL.h \
    frontpanel.h \
    hhmessageevent.h \
    hhdisplaywidget.h \
    hhxmlreader.h \
    hhcommand.h \
    hhwireincommand.h \
    hhdelaycommand.h \
    hhsequencemgr.h \
    hhconstants.h \
    hhmessenger.h \
    hhsliderwidget.h \
    hhsystemcommand.h \
    hhrenderthread.h \
    hhdataqueue.h \
    hhdatareader.h \
    hhdataprocessor.h

FORMS    += mainwindow.ui

win32 {
    CONFIG(debug) {
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\*.xml $$shell_path($$OUT_PWD)\debug\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\*.dll $$shell_path($$OUT_PWD)\debug\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\FPN.txt $$shell_path($$OUT_PWD)\debug\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\*.bit $$shell_path($$OUT_PWD)\debug\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\*.xml $$shell_path($$OUT_PWD)\release\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\*.dll $$shell_path($$OUT_PWD)\release\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\FPN.txt $$shell_path($$OUT_PWD)\release\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\*.bit $$shell_path($$OUT_PWD)\release\
    }
    else {
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\*.xml $$shell_path($$OUT_PWD)\debug\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\*.dll $$shell_path($$OUT_PWD)\debug\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\FPN.txt $$shell_path($$OUT_PWD)\debug\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\*.bit $$shell_path($$OUT_PWD)\debug\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\*.xml $$shell_path($$OUT_PWD)\release\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\*.dll $$shell_path($$OUT_PWD)\release\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\FPN.txt $$shell_path($$OUT_PWD)\release\ &
        QMAKE_POST_LINK += copy $$shell_path($$PWD)\*.bit $$shell_path($$OUT_PWD)\release\
    }
}

#QMAKE_POST_LINK = copy $$shell_path($$PWD)\*.xml $$shell_path($$OUT_PWD)\debug\
