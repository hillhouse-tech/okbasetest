#ifndef HHSYSTEMCOMMAND_H
#define HHSYSTEMCOMMAND_H

#include "hhcommand.h"
#include <stdint.h>

class HHSystemCommand : public HHCommandBase
{
public:
    HHSystemCommand(const QString& name);

    virtual void execute();
    virtual void setValue(uint32_t value);

private:
    void SYSTEM_setLowerADC(uint32_t value);
    void SYSTEM_setUpperADC(uint32_t value);
    void SYSTEM_startRecording(uint32_t value);
    void SYSTEM_stopRecording(uint32_t value);
    void SYSTEM_playback(uint32_t value);
    void SYSTEM_setBrightness(uint32_t value);
    void SYSTEM_setThreshold(uint32_t value);
    void SYSTEM_setContrast(uint32_t value);
    void SYSTEM_setSample(uint32_t value);
    void SYSTEM_advancedSettings(uint32_t value);
    void SYSTEM_reinitializeFPGA(uint32_t value);
    void SYSTEM_generateFPN(uint32_t value);
    void SYSTEM_adjustBrightness(uint32_t value);

private:
    uint32_t mArg;
};

#endif // HHSYSTEMCOMMAND_H
