#include "hhdataqueue.h"
#include <QMutexLocker>

HHDataQueue::HHDataQueue()
    :m_size(0)
{
}

HHDataQueue::~HHDataQueue()
{
}

void HHDataQueue::push(unsigned char *pData, long length)
{
    push(QPair<unsigned char*, long>(pData, length));
}

void HHDataQueue::push(DataSeg& dataIn)
{
    QMutexLocker lock(&m_csPushPop);
    m_queue.enqueue(dataIn);
    m_size += dataIn.second;
}

void HHDataQueue::pop(unsigned char *&pData, long *length)
{
    DataSeg* dataSeg = nullptr;
    pop(dataSeg);

    if (!dataSeg)
    {
        *length = 0;
    }
    else
    {
        pData = dataSeg->first;
        *length = dataSeg->second;
    }

    delete dataSeg;
    dataSeg = nullptr;
}

uint32_t HHDataQueue::size()
{
    QMutexLocker lock(&m_csPushPop);
    return m_size;
}

void HHDataQueue::pop(DataSeg*& dataOut)
{
    QMutexLocker lock(&m_csPushPop);

    if (m_queue.size() <=0 )
        return;

    dataOut = new QPair<unsigned char*, long>(m_queue.head().first, m_queue.head().second);
    m_queue.dequeue();
    m_size -= dataOut->second;
}

void HHDataQueue::clear()
{
    QMutexLocker lock(&m_csPushPop);

    while (m_queue.size() > 0)
    {
        DataSeg dataToDelete;
        dataToDelete = m_queue.head();
        delete [] dataToDelete.first;

        m_queue.dequeue();
    }

    m_size = 0;
}
