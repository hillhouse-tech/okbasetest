#ifndef HHRENDERTHREAD_H
#define HHRENDERTHREAD_H

#include "hhdataqueue.h"
#include "hhdataprocessor.h"
#include <QThread>
#include <QImage>
#include <QFile>
#include <QDataStream>
#include <stdint.h>

class HHRenderThread : public QThread
{
    Q_OBJECT
public:
    HHRenderThread(QObject *parent = 0);
    ~HHRenderThread();
    void addData(unsigned char* data, long length);
    bool setFpnFile(const QString& fpnFile);
    void setMode(int mode);
    void setImageSelect(int whichImage, int select);
    void setIsPlayback(bool bIsPlayback);
    void generateFPN();
    void adjustBrightness();
    void setUpperADC(uint32_t value);
    void setLowerADC(uint32_t value);
    void setSample(uint32_t value);
    uint32_t queueSize();

signals:
    void renderedImage(const QImage& image, int whichDisplay);
    void commandCallback(QString info);

protected:
    void run() override;

public slots:
    void onFinished();
    void handleImageReady(int whichDisplay);
    void handleCommandCallback(QString info);

private:
    bool mAbort;
    QMutex mMutex;
    HHDataQueue mDataQueue;
    HHDataProcessor mDataProcessor;
    bool mIsPlayback;
    int mMode;
    unsigned int mPopCount;
    uint32_t mSample;
};

#endif // HHRENDERTHREAD_H
