#include "hhcommand.h"

HHCommandBase::HHCommandBase(const QString& name)
    :mName(name), mValid(true), mNeedsArg(false), mErrorMessage("")
{

}

HHCommandBase::~HHCommandBase()
{

}

QString HHCommandBase::name()
{
    return mName;
}

void HHCommandBase::valid(bool bValid)
{
    mValid = bValid;
}

bool HHCommandBase::valid()
{
    return mValid;
}

QString HHCommandBase::error()
{
    return mErrorMessage;
}

void HHCommandBase::error(const QString& error)
{
    mErrorMessage = error;
}

void HHCommandBase::needsArg(bool bNeed)
{
    mNeedsArg = bNeed;
}

bool HHCommandBase::needsArg()
{
    return mNeedsArg;
}

HHCommandBase* HHCommandBase::clone()
{
    return nullptr;
}
