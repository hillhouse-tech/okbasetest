#include "hhxmlreader.h"
#include <qdebug.h>
#include "hhconstants.h"
#include <QDomDocument>
#include <QFile>
#include <QCoreApplication>
#include "hhmessenger.h"
#include "hhwireincommand.h"
#include "hhdelaycommand.h"
#include "hhsystemcommand.h"
#include "hhsequencemgr.h"

HHXmlReader::HHXmlReader()
{
}

HHXmlReader::~HHXmlReader()
{
}

bool HHXmlReader::getNumber(const QString& text, uint32_t* pNumber)
{
    bool bOk;
    if (text.contains("0x") || text.contains("0X"))
    {
        *pNumber = text.toULong(&bOk, 16);
    }
    else
    {
        *pNumber = text.toULong(&bOk, 10);
    }

    return bOk;
}

bool HHXmlReader::parse(const QString& filename, QDomDocument* pDom)
{
    QFile file(QCoreApplication::applicationDirPath() + "/" + filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        HHMessenger::getInstance()->sendMessage("Can't open xml file.", EMessageType::error);
        return false;
    }

    QString errorStr;
    int errorLine;
    int errorColumn;

    if (!pDom->setContent(&file, false, &errorStr, &errorLine, &errorColumn))
    {
        HHMessenger::getInstance()->sendMessage(errorStr + "@line:" + QString::number(errorLine) + ",Column:" + QString::number(errorColumn), EMessageType::error);
        return false;
    }

    return true;
}

bool HHXmlReader::importCommands(QVector<HHCommandBase*>& commandList, QDomDocument* pDom)
{
    QDomElement docElem = pDom->documentElement();

    QString rootTag = docElem.tagName();
    if (rootTag != "commands")
    {
        HHMessenger::getInstance()->sendMessage("Can't find commands in xml.", EMessageType::error);
        return false;
    }

    QDomNodeList nodes = docElem.childNodes();
    for (int i = 0; i < nodes.length(); ++i)
    {
        QDomElement elem = nodes.at(i).toElement();
        QString commandName = elem.attribute("name");

        QDomElement commandElem = elem.firstChild().toElement();

        if (commandElem.tagName() == "wirein")
        {
            HHWireinCommand* pCmd = new HHWireinCommand(commandName);
            QDomNodeList params = commandElem.childNodes();
            for (int j = 0; j < params.length(); ++j)
            {
                QDomElement param = params.at(j).toElement();
                QString tagName = param.tagName();
                QString text = param.text();
                uint32_t value;
                bool bOk = getNumber(text, &value);

                if (!bOk)
                {
                    // invalid value
                    pCmd->valid(false);
                    pCmd->error("Invalid param value for " + tagName);
                    HHMessenger::getInstance()->sendMessage("Invalid value:"+text, EMessageType::error);
                }

                if (tagName == "value")
                {
                    pCmd->setValue(value);
                }
                else if (tagName == "address")
                {
                    pCmd->setAddress(value);
                }
                else if (tagName == "mask")
                {
                    pCmd->setMask(value);
                }
                else
                {
                    // unknow tagName
                    HHMessenger::getInstance()->sendMessage("Unknown tageName:"+tagName, EMessageType::error);
                }
            }

            commandList.push_back(pCmd);
        }
        else if (commandElem.tagName() == "wait")
        {
            HHDelayCommand* pCmd = new HHDelayCommand(commandName);
            QDomNodeList params = commandElem.childNodes();
            for (int j = 0; j < params.length(); ++j)
            {
                QDomElement param = params.at(j).toElement();
                QString tagName = param.tagName();
                QString text = param.text();
                uint32_t value;
                bool bOk = getNumber(text, &value);

                if (!bOk)
                {
                    // invalid value
                    pCmd->valid(false);
                    pCmd->error("Invalid param value for " + tagName);
                    HHMessenger::getInstance()->sendMessage("Invalid value:"+text, EMessageType::error);
                }

                if (tagName == "duration")
                {
                    pCmd->setDuration(value);
                }
                else
                {
                    // unknow tagName
                    qDebug() << "Unknown tageName:" << tagName;
                    HHMessenger::getInstance()->sendMessage("Unknown tageName:" + tagName, EMessageType::error);
                }
            }

            commandList.push_back(pCmd);
        }
    }

    qDebug() << "=== Command List ===";
    for (int i = 0; i < commandList.size(); ++i)
    {
        qDebug() << commandList[i]->name();
    }
    qDebug() << "=================";

    return true;
}

bool HHXmlReader::importSequences(HHSequenceMgr* pSeqMgr, QVector<HHSequence*>& sequenceList, QDomDocument* pDom)
{
    QDomElement docElem = pDom->documentElement();

    QString rootTag = docElem.tagName();
    if (rootTag != "sequences")
    {
        HHMessenger::getInstance()->sendMessage("Can't find sequences in xml.", EMessageType::error);
        return false;
    }

    QDomNodeList nodes = docElem.childNodes();
    for (int i = 0; i < nodes.length(); ++i)
    {
        QDomElement elem = nodes.at(i).toElement();

        QString seqName = elem.attribute("name");
        HHSequence* pSeq = new HHSequence(seqName);

        bool bShow = (elem.attribute("show") != "no");
        pSeq->setShow(bShow);

        bool bAdvanced = (elem.attribute("advanced") == "yes");
        pSeq->setAdvanced(bAdvanced);

        QString next = elem.attribute("next");
        if (!next.isEmpty())
        {
            pSeq->setNext(next);
        }

        QDomNodeList commandElems = elem.childNodes();

        for (int j = 0; j < commandElems.length(); ++j)
        {
            QDomElement cmdElem = commandElems.at(j).toElement();
            QString tagName = cmdElem.tagName();
            if (tagName == "command" || tagName == "system")
            {
                HHCommandBase* pCmd = nullptr;
                if (tagName == "command")
                {
                    pCmd = pSeqMgr->getCommandByName(cmdElem.attribute("name"));
                }
                else if (tagName == "system")
                {
                    QString arg = cmdElem.attribute("value");
                    uint32_t value;
                    bool bOk = getNumber(arg, &value);

                    if (bOk)
                    {
                        pCmd = new HHSystemCommand(cmdElem.attribute("name"));
                        pCmd->setValue(value);
                    }
                    else
                    {
                        // invalid value
                        HHMessenger::getInstance()->sendMessage("Invalid system command value:"+ arg, EMessageType::error);
                    }
                }

                if (pCmd)
                {
                    pSeq->addCommand(pCmd);
                }
                else
                {
                    HHMessenger::getInstance()->sendMessage("Command in sequence not found:" + cmdElem.attribute("name"), EMessageType::error);
                }
            }
            else
            {
                // unknow tagName (only command tag allowed)
                HHMessenger::getInstance()->sendMessage("Unknown tageName (only command/system tag allowed):" + tagName, EMessageType::error);

            }
        }

        sequenceList.push_back(pSeq);

    }

    qDebug() << "=== Sequence List ===";
    for (int i = 0; i < sequenceList.size(); ++i)
    {
        qDebug() << sequenceList[i]->name();
    }
    qDebug() << "=================";

    return true;
}

bool HHXmlReader::importSliders(HHSequenceMgr* pSeqMgr, QVector<HHSequence*>& sliderList, QDomDocument* pDom)
{

    QDomElement docElem = pDom->documentElement();

    QString rootTag = docElem.tagName();
    if (rootTag != "sliders")
    {
        HHMessenger::getInstance()->sendMessage("Can't find sliders in xml.", EMessageType::error);
        return false;
    }

    QDomNodeList nodes = docElem.childNodes();
    for (int i = 0; i < nodes.length(); ++i)
    {
        QDomElement elem = nodes.at(i).toElement();
        QString sliderName = elem.attribute("name");
        QString sMin = elem.attribute("min");
        QString sMax = elem.attribute("max");
        QString sInitial = elem.attribute("initial");

        uint32_t min, max, initial;
        if (!getNumber(sMin, &min) || !getNumber(sMax, &max) || !getNumber(sInitial, &initial))
        {
            HHMessenger::getInstance()->sendMessage("Invalid slider min/max/initial value for slider: " + sliderName, EMessageType::error);
            continue;
        }

        HHSequenceSlider* pSeq = new HHSequenceSlider(sliderName, min, max);
        pSeq->setValue(initial);

        bool bShow = (elem.attribute("show") != "no");
        pSeq->setShow(bShow);

        bool bAdvanced = (elem.attribute("advanced") == "yes");
        pSeq->setAdvanced(bAdvanced);


        QDomNodeList commandElems = elem.childNodes();

        for (int j = 0; j < commandElems.length(); ++j)
        {
            QDomElement cmdElem = commandElems.at(j).toElement();
            QString tagName = cmdElem.tagName();
            if (tagName == "command" || tagName == "system")
            {
                QString cmdName = cmdElem.attribute("name");
                HHCommandBase* pCmd = nullptr;
                if (tagName == "command")
                    pCmd = pSeqMgr->getCommandByName(cmdName);
                else if (tagName == "system")
                    pCmd = new HHSystemCommand(cmdName);

                if (cmdName.contains("#value#"))
                    pCmd->needsArg(true);

                bool bHasFixedValue = cmdName.contains("#fixed value#");
                if (bHasFixedValue)
                {
                    pCmd = pCmd->clone();
                }

                if (pCmd)
                {
                    if (pCmd->needsArg() || bHasFixedValue)
                    {
                        QString arg = cmdElem.attribute("value");
                        uint32_t value;
                        bool bOk = getNumber(arg, &value);

                        if (bOk)
                        {
                            pCmd->setValue(value);
                        }
                        else
                        {
                            // invalid value
                            HHMessenger::getInstance()->sendMessage("Invalid slider initial value:"+ arg, EMessageType::error);
                        }
                    }

                    pSeq->addCommand(pCmd);
                }
                else
                {
                    HHMessenger::getInstance()->sendMessage("Command in sequence not found:" + cmdElem.attribute("name"), EMessageType::error);
                }
            }
            else
            {
                // unknow tagName (only command or system tag allowed)
                HHMessenger::getInstance()->sendMessage("Unknown tageName (only command or system tag allowed):" + tagName, EMessageType::error);

            }
        }

        sliderList.push_back(pSeq);

    }

    qDebug() << "=== Slider List ===";
    for (int i = 0; i < sliderList.size(); ++i)
    {
        qDebug() << sliderList[i]->name();
    }
    qDebug() << "=================";

    return true;
}






