#ifndef HHDATAQUEUE_H
#define HHDATAQUEUE_H

#include <queue>
#include <QQueue>
#include <QMutex>
#include <QPair>
#include <stdint.h>

class HHDataQueue
{
public:
    typedef QPair<unsigned char*, long> DataSeg;

    HHDataQueue();
    ~HHDataQueue();

    void push(unsigned char* pData, long length);
    void pop(unsigned char*& pData, long* length);
    uint32_t size();
    void clear();

private:
    void push(DataSeg& dataIn);
    void pop(DataSeg*& dataOut);

private:
    QQueue<DataSeg> m_queue;
    uint32_t m_size;
    QMutex m_csPushPop;
};

#endif // HHDATAQUEUE_H
